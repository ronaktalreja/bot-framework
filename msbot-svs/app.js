var restify = require('restify');
var builder = require('botbuilder');
var cognitiveservices = require('lib/botbuilder-cognitiveservices');

//=========================================================
// Bot Setup
//=========================================================

// Setup Restify Server
var server = restify.createServer();
server.listen(process.env.port || process.env.PORT || 3978, function () {
   console.log('%s listening to %s', server.name, server.url); 
});
  
// Create chat bot
var connector = new builder.ChatConnector({
appId: process.env.MICROSOFT_APP_ID,
appPassword: process.env.MICROSOFT_APP_PASSWORD
});
var bot = new builder.UniversalBot(connector);
server.post('/api/messages', connector.listen());

//=========================================================
// Recognizers
//=========================================================

var qnarecognizer = new cognitiveservices.QnAMakerRecognizer({
	knowledgeBaseId: 'f4b2384c-dc0d-4560-8783-691e7f91f1cb', 
	subscriptionKey: '00095f5c682142e79b8e3763b566399e',
    top: 4});

var model="https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/c413b2ef-382c-45bd-8ff0-f76d60e2a821?subscription-key=1856f2fd0d4945c793dd6a1551de0fec&q=";
var recognizer = new builder.LuisRecognizer(model);

//=========================================================
// Bot Dialogs
//=========================================================
var intents = new builder.IntentDialog({ recognizers: [recognizer, qnarecognizer] });
bot.dialog('/', intents);

intents.matches('luisIntent1', builder.DialogAction.send('Inside LUIS Intent 1.'));

intents.matches('luisIntent2', builder.DialogAction.send('Inside LUIS Intent 2.'));

intents.matches('qna', [
    function (session, args, next) {
        console.log("hello2");
        var answerEntity = builder.EntityRecognizer.findEntity(args.entities, 'answer');
        console.log(answerEntity.entity.split(" "));
        session.send(answerEntity.entity);
    }
]);



// Customer feedback


bot.dialog('feedback', [
    function (session) {
        builder.Prompts.number(session, "How would you rate your experience?");
    },
    function(session, results){
        if(results.response){
            session.dialogData = results.response;
            var msg = 'Thank you for your feedback';
            session.endConversation(msg);
        }
    }
])
.triggerAction({
    matches: /^[Tt]hanks$|^[Oo]k$/i,
});

// Default statement

intents.onDefault([
    function(session){
        session.send('Sorry, no match found');

    }
]);